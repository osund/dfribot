# Commands:
#   .atlas - Search using https://atlas.torproject.org
#   .globe - Search using https://globe.torproject.org
#   .dfri - Show status for Tor relays hosted by DFRI
#
# Author:
#   osund

module.exports = (robot) ->

  #onionoo_url = "https://onionoo.torproject.org"
  onionoo_url = "https://onionoo.thecthulhu.com"

  atlas_url = "https://atlas.torproject.org/#details/"
  globe_url = "https://globe.torproject.org/#/relay/"
  onion_atlas_url = "http://atlas777hhh7mcs7.onion/#details/"
  onion_globe_url = "http://globe223ezvh6bps.onion/#/relay/"

  relay_summary = (msg, service_url) ->
    name = msg.match[2]
    msg.http(onionoo_url + "/summary?search=" + name).get() (err, res, body) ->
        data = JSON.parse body
        relay = data.relays[0]
        running = if relay.r then "ja" else "nej"
        msg.send "#{relay.n} - #{service_url}#{relay.f} - körs: #{running}"

  robot.hear /^\.atlas( name)? (.*)/i, (msg) ->
    relay_summary(msg, atlas_url)

  robot.respond /atlas( name)? (.*)/i, (msg) ->
    relay_summary(msg, atlas_url)

  robot.hear /^\.globe( name)? (.*)/i, (msg) ->
    relay_summary(msg, globe_url)

  robot.respond /globe( name)? (.*)/i, (msg) ->
    relay_summary(msg, globe_url)

  # onion
  robot.hear /^\.oatlas( name)? (.*)/i, (msg) ->
    relay_summary(msg, onion_atlas_url)

  robot.respond /oatlas( name)? (.*)/i, (msg) ->
    relay_summary(msg, onion_atlas_url)

  robot.hear /^\.oglobe( name)? (.*)/i, (msg) ->
    relay_summary(msg, onion_globe_url)

  robot.respond /oglobe( name)? (.*)/i, (msg) ->
    relay_summary(msg, onion_globe_url)

  robot.hear /^\.dfri/i, (msg) ->
    msg.http(onionoo_url + "/summary?search=dfri&contact=dfri").get() (err, res, body) ->

        data = JSON.parse body
        relays = data.relays

        n_relays = relays.length
        n_running = 0

        for r in relays
          if r.r
            n_running++

        msg.send "#{n_running} av DFRI:s #{n_relays} Tor-relän verkar köras."
